
docker build . -t test
docker run --rm -d --user root:root -p 2222:2222 -e "PASSWORD=password" test bash -c "sleep 200"
docker exec -it --user 1001:root -w /opt/app-root/src -e"HOME=/opt/app-root/src" -e "PASSWORD=password" ssh bash
docker run -it --user root:root --net=host -w /opt/app-root/src -e"HOME=/opt/app-root/src" -e "PASSWORD=password" test bash 
/usr/bin/container-entrypoint sleep 2
ssh -i ~/.ssh/id_rsa root@127.0.0.1 -p 2222 -vv

docker run -P --user 1001:root -e "PASSWORD=password" \
    -e "AUTHORIZED_KEY=ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEAqL4/o1ZWgahhONfev7pf2nAGR7XFyBDehxJBds4TFYhm2qOcmZWv18WWvmChvNcuPqfqSaziLLlxFLMg+1YqVDL0TQ6Myk8vZgdl5vJ2idLgOEcLSqumWQxZWy98FyY7I2VRhlUMq/ICJHRLtSTEO+q+2bmnImjpm5Kxdch3b7ptD05/gqJtTvUHgy8eepp8/wD9+T+Za1a8rTXiENiHPTr42JTzsTC3mOMLDG5m9fFKp92Q6fMYnCNSQbgNbVTQpXaQCvE2hDrp3q2Xb4NUdQwlwyX150Nv1DCqZrkIAZGR/vKEFdGzuOz+pcG7q6X8qvTKKzDqJ2JQ9RXYB4KE5w== bjoern@xrow.de" \
    ssh

docker run -P --user root:root -e "PASSWORD=password" \
    -e "AUTHORIZED_KEY=ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEAqL4/o1ZWgahhONfev7pf2nAGR7XFyBDehxJBds4TFYhm2qOcmZWv18WWvmChvNcuPqfqSaziLLlxFLMg+1YqVDL0TQ6Myk8vZgdl5vJ2idLgOEcLSqumWQxZWy98FyY7I2VRhlUMq/ICJHRLtSTEO+q+2bmnImjpm5Kxdch3b7ptD05/gqJtTvUHgy8eepp8/wD9+T+Za1a8rTXiENiHPTr42JTzsTC3mOMLDG5m9fFKp92Q6fMYnCNSQbgNbVTQpXaQCvE2hDrp3q2Xb4NUdQwlwyX150Nv1DCqZrkIAZGR/vKEFdGzuOz+pcG7q6X8qvTKKzDqJ2JQ9RXYB4KE5w== bjoern@xrow.de" \
    ssh

docker exec -it --user 1001:root  -e "PASSWORD=password" ssh bash
