## DinD

Possibl fixes
podman run -it --privileged -v ./test:/tmp/src -v ~/containers:/var/lib/containers:Z --device=/dev/fuse registry.gitlab.com/xrow-public/ci-tools/tools:3.0 sh
+
podman --events-backend=file info

### Working sets

podman run --rm quay.io/podman/stable:master bash -c "podman --storage-driver=vfs info"
podman run --rm quay.io/podman/stable:v2.1.1 bash -c "podman --storage-driver=vfs info"
podman run --rm registry.gitlab.com/xrow-public/ci-tools/tools:3.0 bash -c "podman --storage-driver=vfs info"
podman run --rm quay.io/podman/stable:v2.1.1 bash -c "podman --storage-driver=vfs info"

podman run -v ./:/tmp/build --privileged --rm -it quay.io/buildah/stable buildah build-using-dockerfile -t test -f /tmp/build/Dockerfile

podman run -v ./:/tmp/build --privileged --rm -it quay.io/podman/stable:v2.1.1 podman build -t test -f /tmp/build/Dockerfile

podman run -v ./:/tmp/build --privileged --rm -it registry.gitlab.com/xrow-public/ci-tools/tools:3.0 podman --storage-driver=vfs build -t test -f /tmp/build/Dockerfile

### Working with warning

Warning: time="2020-11-28T18:23:30Z" level=error msg="unable to write system event: \"write unixgram @0006b->/run/systemd/journal/socket: sendmsg: no such file or directory\""

Workaround: --events-backend=file

podman run --rm --env "STORAGE_DRIVER=vfs" registry.gitlab.com/xrow-public/ci-tools/tools:3.0 bash -c "podman info"
podman run --rm registry.gitlab.com/xrow-public/ci-tools/tools:3.0 bash -c "podman --storage-driver=vfs info"

### Errors: Build with s2i

ERRO[0000] container_linux.go:349: starting container process caused "error adding seccomp rule for syscall socket: requested action matches default action of filter" 
container_linux.go:349: starting container process caused "error adding seccomp rule for syscall socket: requested action matches default action of filter"
error running container: error creating container for [/bin/sh -c chown -R 1001:0 /tmp/src]: : exit status 1
Error: error building at STEP "RUN chown -R 1001:0 /tmp/src": error while running runtime: exit status 1

s2i]# podman build --no-cache --force-rm -t test -f Dockerfile 

Workaround: buildah build-using-dockerfile -t test -f Dockerfile

### Errors: Other

* ?

Error: mount /var/lib/containers/storage/overlay:/var/lib/containers/storage/overlay, flags: 0x1000: operation not permitted

podman run --rm --env STORAGE_DRIVER=overlay --env STORAGE_OPTS=overlay2.mount_program=/usr/bin/fuse-overlayfs quay.io/podman/stable:master bash -c "podman info"

* Yaml error in some internal file

ERRO[0000] Error retrieving container 1ee11219dcde23dc37c1378a661d242239d9e7fe14055bf40a298e4f5cbbdc77 from the database: error unmarshalling container 1ee11219dcde23dc37c1378a661d242239d9e7fe14055bf40a298e4f5cbbdc77 config: libpod.ContainerConfig.ContainerRootFSConfig: ImageVolumes: []*libpod.ContainerImageVolume: decode slice: expect [ or n, but found t, error found in #10 byte of ...|Volumes":true,"ShmDi|..., bigger context ...|e":"quay.io/podman/stable:v2.1.1","imageVolumes":true,"ShmDir":"/var/lib/containers/storage/overlay-|... 

https://stackoverflow.com/questions/57233686/error-when-creating-deployment-yaml-deployment-in-version-v1-cannot-be-hand/57233917

[root@localhost s2i]# podman run --rm --privileged -it registry.gitlab.com/xrow-public/docker-php-ezplatform:3.0.16 sh

* ?

panic: runtime error: invalid memory address or nil pointer dereference
[signal SIGSEGV: segmentation violation code=0x1 addr=0x28 pc=0x5591f23e6836]

podman run --rm -v ./:/tmp --env "STORAGE_DRIVER=vfs" quay.io/podman/stable:master bash -c "podman build --no-cache -t dind-test -f /tmp/Dockerfile"

* Node goes crazy

Error: mount /var/lib/containers/storage/overlay:/var/lib/containers/storage/overlay, flags: 0x1000: operation not permitted
podman run --rm -v ./:/tmp quay.io/podman/stable:master bash -c "podman build --no-cache -t dind-test -f /tmp/Dockerfile"

100GB Memory during build

[851.2MiB/599.68s] Executing script yarn install
                                                                   
  [Symfony\Component\Process\Exception\ProcessTimedOutException]   
  The process "yarn install" exceeded the timeout of 300 seconds. 

[root@server005 ~]# top
top - 05:10:39 up 13 days, 13:55,  2 users,  load average: 1.87, 1.44, 1.21
Tasks: 2620 total,   2 running, 2618 sleeping,   0 stopped,   0 zombie
%Cpu(s): 14.0 us,  1.6 sy,  0.0 ni, 84.1 id,  0.0 wa,  0.2 hi,  0.1 si,  0.0 st
MiB Mem :  96404.9 total,   1105.1 free,  34996.1 used,  60303.7 buff/cache
MiB Swap:  20480.0 total,  20315.2 free,    164.8 used.  60302.2 avail Mem 

    PID USER      PR  NI    VIRT    RES    SHR S  %CPU  %MEM     TIME+ COMMAND                          
1750106 1001      20   0 1076696 293452  30276 R 170.3   0.3   0:26.68 node                             
1749814 root      20   0   70264   8008   4500 R   2.0   0.0   0:00.49 top   

[centos@localhost root]$ podman info
Error: could not get runtime: error generating default config from memory: cannot mkdir /run/user/0/libpod: mkdir /run/user/0/libpod: permission denied

## Testing

podman run --rm registry.gitlab.com/xrow-public/docker-php-ezplatform/master bash

podman run --rm -v ./:/tmp --device /dev/fuse -v /tmp/test:/var/lib/containers/storage:Z quay.io/podman/stable:master bash -c "podman build --no-cache -t dind-test -f /tmp/Dockerfile"

podman run --device /dev/fuse -v ./:/tmp/build -v /tmp/test:/var/lib/containers/storage:Z --user build --rm -it quay.io/buildah/stable buildah build-using-dockerfile -t test /tmp/build



[root@localhost podman]# podman run -it  quay.io/podman/stable:v2.1.1 podman info
Error: could not get runtime: Unable to parse key/value option: vfs
[root@localhost podman]# podman run -it --env "STORAGEOPTS=vfs" podman podman info 
ERRO[0000] 'overlay' is not supported over overlayfs    
Error: could not get runtime: 'overlay' is not supported over overlayfs: backing file system is unsupported for this graph driver

quay.io/podman/stable:v2.1.1