<?php

$file = preg_replace( '#(/v[^/]*)?[/]*(.*)#i', '${2}',$_SERVER["REQUEST_URI"]);
$version = preg_replace( '#(/v[^/]*)?[/]*(.*)#i', '${1}',$_SERVER["REQUEST_URI"]);
if(file_exists( $file ) ){
   $contents = file_get_contents( $file );
   $contents = preg_replace('#(.*)(gitlab\\.com)(/xrow-public/ci-tools/raw/)([^/]*)(.*)#i', '${1}'. $_SERVER['HTTP_HOST']. $version . '${5}', $contents, -1);
   header('Content-Type: text/plain');
   echo $contents;
}
else
{
   die("GitLaB template file not found.");
}
exit(0);