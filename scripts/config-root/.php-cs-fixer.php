<?php

$config = new PhpCsFixer\Config();

return $config->setRules([
        '@Symfony' => true,
        '@Symfony:risky' => true,
        'concat_space' => ['spacing' => 'one'],
        'array_push' => true,
        'array_syntax' => ['syntax' => 'short'],
        'simplified_null_return' => false,
        'native_function_invocation' => false,
        'phpdoc_align' => false,
        'phpdoc_separation' => false,
        'phpdoc_to_comment' => false,
        'cast_spaces' => false,
        'blank_line_after_opening_tag' => false,
        'linebreak_after_opening_tag' => true,
        'logical_operators' => true,
        'single_space_after_construct' => true,
        'single_blank_line_before_namespace' => false,
        'phpdoc_annotation_without_dot' => false,
        'phpdoc_no_alias_tag' => false,
        'protected_to_private' => true,
        'no_superfluous_phpdoc_tags' => false,
        'space_after_semicolon' => false,
        'yoda_style' => false,
        'no_break_comment' => false,
        'no_trailing_whitespace' => true,
    ])
    ->setRiskyAllowed(true)
    ->setFinder(
        PhpCsFixer\Finder::create()
            ->in(__DIR__)
            ->exclude([
                'vendor',
                'bin/.ci',
                'bin/.travis',
                'doc',
                'app/cache',
                'var/cache',
                'ezpublish_legacy',
            ])
            ->files()->name('*.php')
    )
;
