# GitLab Pipelines

Include a stable pipeline

```yaml
include:
  - remote: 'https://gitlab.com/xrow-public/ci-tools/raw/1.0/.gitlab-php-s2i-ci-template.yml'
```

Include a latest pipeline

```yaml
include:
  - remote: 'https://gitlab.com/xrow-public/ci-tools/raw/3.0/.gitlab-php-s2i-ci-template.yml'
```